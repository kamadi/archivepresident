<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Awaken
 */
?>
</div><!-- container -->
</div><!-- #content -->
<div class="footer-ornament">
    <div class="container">
        </div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="container">
        <div class="row">
            <div class="footer-carousel">
                <link rel='stylesheet' id='owl-carousel-css'
                      href='<?php echo plugins_url('carousel-slider/public/owl-carousel/owl.carousel.css?ver=2.0.0')?>'
                      type='text/css' media='all'/>
                <link rel='stylesheet' id='carousel-slider-css'
                      href='<?php echo plugins_url('carousel-slider/public/owl-carousel/style.css?ver=1.4.1')?>'
                      type='text/css' media='all'/>
                <script type='text/javascript'
                        src="<?php echo plugins_url('carousel-slider/public/owl-carousel/owl.carousel.min.js?ver=2.0.0')?>"></script>
                <?php echo do_shortcode('[carousel_slide id="32"]'); ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="footer-widget-area">
                <div class="col-md-4">
                    <div class="left-footer">
                        <div class="widget-area" role="complementary">
                            <?php if (!dynamic_sidebar('footer-left')) : ?>

                            <?php endif; // end sidebar widget area ?>
                        </div>
                        <!-- .widget-area -->
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="mid-footer">
                        <div class="widget-area" role="complementary">
                            <?php if (!dynamic_sidebar('footer-mid')) : ?>

                            <?php endif; // end sidebar widget area ?>
                        </div>
                        <!-- .widget-area -->                        </div>
                </div>

                <div class="col-md-4">
                    <div class="right-footer">
                        <div class="widget-area" role="complementary">
                            <?php if (!dynamic_sidebar('footer-right')) : ?>

                            <?php endif; // end sidebar widget area ?>
                        </div>
                        <!-- .widget-area -->
                    </div>
                </div>
            </div>
            <!-- .footer-widget-area -->
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->

    <div class="footer-site-info">
        <div class="container">
            <div class="row">
                <?php
                $footer_copyright_text = get_theme_mod('footer_copyright_text', '');
                if (!empty($footer_copyright_text)) {
                    echo wp_kses_post($footer_copyright_text);
                } else {
                    ?>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <?php bloginfo('name'); ?>
                    </div>
                <?php } ?>
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </div>
    <!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
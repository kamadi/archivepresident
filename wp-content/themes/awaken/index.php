<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awaken
 */

get_header(); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3">
        <?php //get_template_part('right-menu'); ?>
        <?php wp_nav_menu(array('theme_location' => 'left_menu','menu_class'=>'left-menu tree'));?>
    </div>

    <!-- .bootstrap cols -->    <?php is_rtl() ? $rtl = 'awaken-rtl' : $rtl = ''; ?>
    <div class="col-xs-12 col-sm-12 col-md-6 <?php echo $rtl ?>">
        <?php
        if (is_front_page()) {
            ?>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <?php
                    //                if (get_theme_mod('display_slider', 1) == '1') {
                    //                    awaken_featured_posts();
                    //                }
                    $slider_posts = new WP_Query(array(
                            'posts_per_page' => 4,
                        )
                    );
                    ?>
                    <ul class="pgwSlider">
                        <?php
                        while ($slider_posts->have_posts()) :
                            $slider_posts->the_post(); ?>
                            <li>
                                <a href="<?php echo get_permalink() ?>">
                                    <?php if (has_post_thumbnail()) { ?>
                                        <?php the_post_thumbnail('featured-slider', array('alt' => get_the_title())); ?>
                                    <?php } else { ?>
                                        <img src="<?php echo get_template_directory_uri() . '/images/slide.jpg' ?>">
                                    <?php } ?>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 latest-news">
                    <h3><?php echo pll__('Latest news'); ?></h3>
                    <ul>
                        <?php
                        if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <li>
                                    <?php the_title(sprintf('<a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a>'); ?>
                                    <p><?php echo get_the_date() ?></p>
                                </li>
                            <?php endwhile; ?>
                        <?php endif;
                        ?>
                    </ul>
                </div>
            </div>
        <?php
        }
        ?>
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php if (have_posts()) : ?>

                    <?php /* Start the Loop */
                    $counter = 0;
                    ?>

                    <div class="row">
                        <?php while (have_posts()) : the_post(); ?>

                            <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part('content', get_post_format());
                            ?>

                            <?php
                            $counter++;
                            if ($counter % 2 == 0) {
                                echo '</div><div class="row">';
                            }
                            ?>
                        <?php endwhile; ?>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <?php awaken_paging_nav(); ?>
                        </div>
                    </div><!-- .row -->

                <?php else : ?>

                    <?php get_template_part('content', 'none'); ?>

                <?php endif; ?>

            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .bootstrap cols -->
    <div class="col-xs-12 col-sm-6 col-md-3">
        <?php get_sidebar(); ?>
    </div>
</div><!-- .row -->
<?php get_footer(); ?>

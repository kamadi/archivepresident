<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Awaken
 */

if (!is_active_sidebar('sidebar-1')) {
    return;
}
?>

<div id="secondary" class="main-widget-area" role="complementary">
    <div class="right-sidebar">
        <div class="awaken-search-box">
            <form action="<?php echo esc_url(home_url('/')); ?>" id="awaken-search-form" method="get">
                <input type="text" value="" name="s" id="s"/>
                <input type="submit" value="<?php echo pll__('Search'); ?>"/>
            </form>
        </div>
        <a href="http://asharshylyq.kz/?lang=kk/"  style=" ">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/president.jpg" title="Послание Президента"
                 style="width:80%;margin-bottom: 10px;;border-radius: 7%;margin-left: 10%" >
        </a>
        <a href="<?php echo get_site_url(); ?>"  style="">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/director.jpeg" title="Блог директора"
                 style="width:80%;margin-bottom: 10px;;border-radius: 7%;margin-left: 10%" >
        </a>
        <div class="textwidget">
            <h3 style="text-transform: uppercase;text-align: center">Проекты архива</h3>
            <a href="http://asharshylyq.kz/?lang=kk/" style="margin-left: 23% ">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/asharshylyq.jpg" alt="asharshylyq"
                     style="width:56%;margin-bottom: 10px;border-radius: 7%" >
            </a>
            <a href="http://tutkyn.kz/?lang=kk" style="margin-left: 23% ">

                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/tutkyn.jpg"
                     style="width:56%;margin-bottom: 10px;;border-radius: 7%" >
            </a>
            <a href="http://archive.president.kz/кеңестік-қазақстанның-поменклатурал" style="margin-left: 23% ">

                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nomeklatura.jpg"
                     style="width:56%;margin-bottom: 10px;;border-radius: 7%" >
            </a>
        </div>
        <?php dynamic_sidebar('sidebar-1'); ?>
<!--        <span id="hotlog_counter"></span>-->
<!--        <span id="hotlog_dyn"></span>-->
<!--        <script type="text/javascript"> var hot_s = document.createElement('script');-->
<!--            hot_s.type = 'text/javascript';-->
<!--            hot_s.async = true;-->
<!--            hot_s.src = 'http://js.hotlog.ru/dcounter/2532402.js';-->
<!--            hot_d = document.getElementById('hotlog_dyn');-->
<!--            hot_d.appendChild(hot_s);-->
<!--        </script>-->
<!--        <noscript>-->
<!--            <a href="http://click.hotlog.ru/?2532402" target="_blank">-->
<!--                <img src="http://hit19.hotlog.ru/cgi-bin/hotlog/count?s=2532402&im=402" border="0"-->
<!--                     title="HotLog" alt="HotLog"></a>-->
<!--        </noscript>-->
    </div>
</div><!-- #secondary -->

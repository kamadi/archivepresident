<ul class="left-menu">
    <?php

    $menu_name = 'left_menu';
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menuitems = wp_get_nav_menu_items($menu_name);

    $count = 0;
    $submenu = false;

    foreach ($menuitems as $item):
        // set up title and url
        $title = $item->title;
        $link = $item->url;

        // item does not have a parent so menu_item_parent equals 0 (false)
        if (!$item->menu_item_parent):

            // save this id for later comparison with sub-menu items
            $parent_id = $item->ID;
            ?>
            <li class="item">
            <a href="<?php echo $link; ?>" class="title">
                <?php echo $title; ?>
            </a>
        <?php endif; ?>
        <?php if ($parent_id == $item->menu_item_parent): ?>
        <?php if (!$submenu): $submenu = true; ?>
            <ul class="sub-menu">
        <?php endif; ?>
        <li class="item">
            <a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a>
        </li>
        <?php if ($menuitems[$count + 1]->menu_item_parent != $parent_id && $submenu): ?>
            </ul>
            <?php $submenu = false; endif; ?>

    <?php endif; ?>
        <?php if ($menuitems[$count + 1]->menu_item_parent != $parent_id): ?>
        </li>
        <?php $submenu = false; endif; ?>

        <?php $count++; endforeach; ?>

</ul>
<script>
//    jQuery(document).ready(function(){
//        var $ = jQuery;
//       $('.left-menu li a').click(function(){
//           if($(this).parent().has($('.sub-menu'))){
//               $(this).parent().find('.sub-menu').toggle();
//           }
//       });
//    });
</script>
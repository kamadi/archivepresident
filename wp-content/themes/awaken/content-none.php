<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awaken
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php echo pll__('Nothing Found'); ?></h1>
	</header><!-- .page-header -->
</section><!-- .no-results -->
